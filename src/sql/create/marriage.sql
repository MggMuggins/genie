CREATE TABLE Marriage (
    dates       INTEGER REFERENCES DateRange(id),
    spouse_x    INTEGER REFERENCES Person(id),
    spouse_y    INTEGER REFERENCES Person(id)
);

