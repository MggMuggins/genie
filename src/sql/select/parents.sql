select
    ParentX.givname,
    ParentX.surname,
    DRX.start_day,
    DRX.start_month,
    DRX.start_year,
    DRX.end_day,
    DRX.end_month,
    DRX.end_year,
    
    ParentY.givname,
    ParentY.surname,
    DRY.start_day,
    DRY.start_month,
    DRY.start_year,
    DRY.end_day,
    DRY.end_month,
    DRY.end_year
from Person
    join Person as ParentX on ParentX.id = Person.parent_x
    join Person as ParentY on ParentY.id = Person.parent_y
    join DateRange as DRX on ParentX.dates = DRX.id
    join DateRange as DRY on ParentY.dates = DRY.id
where Person.givname = (?1)

