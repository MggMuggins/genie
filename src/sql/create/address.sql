CREATE TABLE Address (
    id          INTEGER PRIMARY KEY,
    
    line1       TEXT,
    line2       TEXT,
    city        TEXT,
    region      TEXT,
    country     TEXT NOT NULL,
    postal_code TEXT
);

