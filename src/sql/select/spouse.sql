select
    p.givname,
    sp.givname,
    dr.start_day,
    dr.start_month,
    dr.start_year
from Person p
    join Marriage m on m.spouse_x = p.id
    join Person sp on m.spouse_y = sp.id
    join DateRange dr on m.dates = dr.id

