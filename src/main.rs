use std::fmt::{self, Display, Write};
use std::fs;
use std::path::Path;

use anyhow::Result;
use rusqlite::{Connection, params, Row};

macro_rules! exec_queries {
    ($con:ident, $sql:literal) => {
        println!("Executing {}", $sql);
        $con.execute(include_str!($sql), rusqlite::NO_PARAMS)?;
    };
    ($con:ident, $sql:literal, $($rest:literal),+ $(,)?) => {
        println!("Executing {}", $sql);
        $con.execute(include_str!($sql), rusqlite::NO_PARAMS)?;
        exec_queries!($con, $($rest),+);
    };
}

fn create_db(f: &Path) -> Result<Connection> {
    let con = Connection::open(f)?;
    
    exec_queries!(
        con,
        "sql/create/date_range.sql",
        "sql/create/address.sql",
        "sql/create/person.sql",
        "sql/create/marriage.sql",
        "sql/create/lived_at.sql",
        "sql/insert/date_range.sql",
        "sql/insert/address.sql",
        "sql/insert/person.sql",
        "sql/insert/marriage.sql",
        "sql/insert/lived_at.sql",
    );
    Ok(con)
}

const MONTH_ABB: [&str; 12] = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
];

#[non_exhaustive]
#[allow(non_camel_case_types)]
#[derive(Clone, Copy)]
enum DateFmt {
    D_Mon_YYYY,
}

impl DateFmt {
    fn fmt_date(self, d: Option<u32>, m: Option<u32>, y: Option<u32>) -> String {
        let mut rslt = String::new();
        match self {
            DateFmt::D_Mon_YYYY => {
                if let Some(d) = d {
                    let _ = write!(rslt, "{} ", d);
                }
                if let Some(m) = m {
                    let _ = write!(rslt, "{}, ", MONTH_ABB[m as usize - 1]);
                }
                if let Some(y) = y {
                    let _ = write!(rslt, "{}", y);
                }
            }
        }
        rslt
    }
}

#[derive(Debug)]
struct DateRng {
    start_day: Option<u32>,
    start_month: Option<u32>,
    start_year: u32,

    end_day: Option<u32>,
    end_month: Option<u32>,
    end_year: Option<u32>,
}

impl DateRng {
    fn from_row(row: &Row, at: usize) -> Result<DateRng, rusqlite::Error> {
        Ok(DateRng {
            start_day: row.get(at)?,
            start_month: row.get(at + 1)?,
            start_year: row.get(at + 2)?,
            end_day: row.get(at + 3)?,
            end_month: row.get(at + 4)?,
            end_year: row.get(at + 5)?,
        })
    }
    
    fn fmt_start(&self, fmt: DateFmt) -> String {
        fmt.fmt_date(self.start_day, self.start_month, Some(self.start_year))
    }
    
    fn fmt_end(&self, fmt: DateFmt) -> String {
        fmt.fmt_date(self.end_day, self.end_month, self.end_year)
    }
}

#[derive(Debug)]
struct Person {
    givname: String,
    surname: String,
    dates: DateRng,
}

impl Person {
    // A person shows up in a query as (givname, surname, DateRange)
    fn from_row(row: &Row, at: usize) -> Result<Person, rusqlite::Error> {
        Ok(Person {
            givname: row.get(at)?,
            surname: row.get(at + 1)?,
            dates: DateRng::from_row(row, at + 2)?,
        })
    }
}

impl Display for Person {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}; b. {}; ",
            self.givname,
            self.surname,
            self.dates.fmt_start(DateFmt::D_Mon_YYYY),
        )?;
        
        let death_date = self.dates.fmt_end(DateFmt::D_Mon_YYYY);
        if &death_date != "" {
            write!(f, "d. {}; ", death_date)?;
        }
        Ok(())
    }
}

fn ancestor_tree(pers: Person, conn: &Connection) -> Result<termtree::Tree<Person>> {
    let mut sql = conn.prepare(include_str!("sql/select/parents.sql"))?;
    let mut parents_iter = sql.query(params![pers.givname])?
        .mapped(|row| {
            let px = Person::from_row(row, 0)?;
            let py = Person::from_row(row, 8)?;
            Ok((px, py))
        });
    
    let parents = if let Some((px, py)) = parents_iter.next().transpose()? {
        vec![ancestor_tree(px, conn)?, ancestor_tree(py, conn)?]
    } else {
        vec![]
    };
    Ok(termtree::Tree::new(pers, parents))
}

fn main() -> Result<()> {
    let db_file = &Path::new("genio.sqlite");
    let db_file_meta = fs::metadata(db_file)?;
    
    let conn = if db_file_meta.is_file() {
         Connection::open(db_file)?
    } else {
        create_db(db_file)?
    };
    
    let mut person_stmt = conn.prepare(include_str!("sql/select/person.sql"))?;
    let person_iter = person_stmt.query(params!["%Wesley%"])?
       .mapped(|row| Person::from_row(row, 0) );
    
    for p in person_iter {
        let p = p?;
        println!("{}", ancestor_tree(p, &conn)?);
    }
    
    Ok(())
}

