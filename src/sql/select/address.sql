-- Current address for a person
select
    a.line1,
    a.line2,
    a.city,
    a.region,
    a.postal_code
from Address a
    join LivedAt l on l.address = a.id
    join DateRange d on d.id = l.dates
    join Person p on p.id = l.person
where d.end_day is null
    and d.end_month is null
    and d.end_year is null
    and p.givname like "%Wesley%"

