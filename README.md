# Genie
SQLite tool for messing around with geneologies. CSC480 project.

# Structure
- `class_diagram.pdf`
- `src/sql/create/*` for the spec
- `src/sql/select/*` for a couple of useful(?) queries

I wrote a couple of insert queries as well with my family's data. Those aren't
included for obvious reasons.

# Design
Obviously families are complicated (even though my immediate family isn't), so I
wanted to make sure to handle some of the hairier aspects well. I'm not sure I
pulled this off entirely, but here are some of the situations that I wanted to
make sure to take care of:
- Adopted Children
- Children born out of wedlock
- Serial marriage (divorce and remarriage)
- Same-sex marriage and parents

First off, the Person table doesn't include a gender field since the primary
goal for the project is to represent relationships between people. I chose to
include two sets of parent fields in the person table, one for biological
parents and one for just parents. This doesn't really represent step-parents,
although that relationship is represented with via marriage with a parent. This
is one area I think could be improved. When naming generally gendered fields, I
chose to use `x` and `y` instead of numbering (to avoid priveleging one over
another) or directly gendering them. Usually an `*_x` field holds the ID of a
female and `*_y` for male IDs, but I figured those labels don't have to mean
male or female and would not have to be used that way. I'd prefer something
entirely neutral, but needed some way to differentiate.

DateRange can hold a starting date and potentially an ending date (end fields
are nullable). This means that we can elegantly represent both the beginning and
duration of something. I'm not sure that NULLs were the right choice here, I
don't remember the query rules and didn't think through the possible
consequences of this decision.

Marriage is a relationship table between two people with a date range. This
allows a marriage to have not ended, or to end at a given date, and for the same
individual to be in more than one marriage. A possible integrity check here
would be to prevent someone being married to more than one person at a time.

Address and LivedAt simply allows a person to have lived at multiple locations
over the course of their life and to record that info in detail. I chose to name
the fields in address to be agnostic when it comes to country, so `region` for
state and `postal_code` instead of zip. Not sure how well this maps to places
outside of the US and Canada.

A lot of fields are nullable because we need to be able to represent incomplete
information. We may know which country somebody lived in but not their specific
region or address in that country. Once again, not sure if NULL is the way to go
here.

My dad got inspired to work on this problem as well, he designed his tables with
a relationship table that has a char field to represent the type of
relationship: marriage, father, mother, etc. I think this is a more elegant way
of dealing with the gender issues above.

