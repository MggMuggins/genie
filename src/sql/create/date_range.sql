CREATE TABLE DateRange (
    id          INTEGER PRIMARY KEY,
    
    start_day   INTEGER check(start_day > 0 AND start_day <= 31),
    start_month INTEGER check(start_month > 0 AND start_month <= 12),
    start_year  INTEGER NOT NULL,
    
    end_day     INTEGER check(end_day > 0 AND end_day <= 31),
    end_month   INTEGER check(end_month > 0 AND end_month <= 12),
    end_year    INTEGER
)

