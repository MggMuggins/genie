CREATE TABLE LivedAt (
    person      INTEGER REFERENCES Person(id),
    dates       INTEGER REFERENCES DateRange(id),
    address     INTEGER REFERENCES Address(id)
);

