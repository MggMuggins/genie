CREATE TABLE Person (
    id          INTEGER PRIMARY KEY,
    givname     TEXT NOT NULL,
    surname     TEXT NOT NULL,
    
    dates       INTEGER REFERENCES DateRange(id),
    
    bio_father  INTEGER REFERENCES Person(id),
    bio_mother  INTEGER REFERENCES Person(id),
    parent_x    INTEGER REFERENCES Person(id),
    parent_y    INTEGER REFERENCES Person(id),
    
    address     INTEGER REFERENCES Address(id)
);

