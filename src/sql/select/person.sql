select
    p.givname,
    p.surname,
    dr.start_day,
    dr.start_month,
    dr.start_year,
    dr.end_day,
    dr.end_month,
    dr.end_year
from Person p
    join DateRange dr on p.dates = dr.id
where p.givname like (?1)

